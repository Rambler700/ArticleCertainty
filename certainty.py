# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 13:53:22 2018

@author: enovi
"""

import nltk

#Certainty analyzer
#Calculate the certainty of the text

def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Certainty Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             analyze_text(article_text)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def analyze_text(article_text):             
    
    cert_list    = ['absolute', 'accurate', 'affirm', 'avow', 'blatant', 'bond', 'clear', 'commit',
                    'confirm', 'correct', 'decide', 'distinct', 'endorse', 'evident', 'false',
                    'frank', 'frank', 'glaring', 'guarantee', 'maintain', 'manifest', 'mark', 
                    'metric', 'oath', 'obvious', 'overt', 'palpable', 'plain', 'pledge', 'precise',
                    'promise', 'pronounced', 'proof', 'proven', 'rank', 'ratify', 'right', 
                    'statistic', 'striking', 'swear', 'transparent', 'troth', 'true', 'valid',
                    'visible', 'vow']
    
    uncert_list  = ['accuse', 'allege', 'ambiguity', 'ambiguous', 'arguable', 'chancy',
                    'claim', 'contend', 'contentious', 'controversial', 'controversy', 'debatable',
                    'dicey', 'disputable', 'dispute', 'dubious', 'equivocal', 'experiment',
                    'faltering', 'foggy', 'hazy', 'hesitant', 'hesitate', 'iffy', 'maintain',
                    'maybe', 'moot', 'mysterious', 'obscure', 'perhaps', 'preliminary', 'possible',
                    'probably', 'questionable', 'risky', 'tentative', 'vacillating', 'vague']
    
    cert_count   = 0
    uncert_count = 0
    
    article_text = article_text.lower().split()
    
    for word in article_text:
        word = nltk.PorterStemmer().stem(word)
    
    for word in cert_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in uncert_list:
        word = nltk.PorterStemmer().stem(word)    
    
    for word in article_text:
        if word in cert_list:
            cert_count += 1
        if word in uncert_list:
            uncert_count += 1
            
    if cert_count >= 1.66 * uncert_count:
        print('The text was very certain.')        
    elif cert_count < 1.66 * uncert_count and cert_count >= 1.25 * uncert_count:
        print('The text was moderately certain.')         
    elif cert_count < 1.25 * uncert_count and cert_count > 0.80 * uncert_count:
        print('The text was balanced.')
    elif cert_count <= 0.80 * uncert_count and cert_count > 0.60 * uncert_count:    
        print('The text was moderately uncertain.')
    elif cert_count <= 0.60 * uncert_count:
        print('The text was very uncertain.')
    else:    
        print('The text did not contain listed certainty keywords.')
            
main()